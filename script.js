"use strict";

//create table
const table = document.createElement('table');


  // row X cell
for (let i = 0; i < 30; i++) {
    const row = document.createElement('tr');
    row.addEventListener('click', () => {
      row.classList.toggle('black');
  });
    for (let j = 0; j < 30; j++) {
        const cell = document.createElement('td');
        cell.addEventListener('click', () => {
            cell.classList.toggle('black');
        });
        row.append(cell);
    }
    table.append(row);
}
document.body.append(table);

// end table create



document.body.addEventListener('click', () => {
    table.classList.toggle('black');
});